﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class UserType
    {
        [Key]
        public int UserTypeId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The maximun length for field {0} is {1} characters")]
        [Index("UserType_Name_Index", IsUnique = true)]
        [Display(Name = "User type")]
        public string Name { get; set; }

        //Relationship between User and UserType (( 1 UserType belong to * Users ))
        public virtual ICollection<User> Fans { get; set; }
    }
}
