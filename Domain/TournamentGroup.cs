﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class TournamentGroup
    {
        #region Properties

        [Key]
        public int TournamentGroupId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The maximum length for field {0} is {1} characters")]
        [Index("TournamentGroup_Name_TournamentId_Index", IsUnique = true, Order = 1)]
        [Display(Name = "Group")]
        public string Name { get; set; }
        
        [Index("TournamentGroup_Name_TournamentId_Index", IsUnique = true, Order = 2)]
        [Display(Name = "Tournament")]
        public int TournamentId { get; set; }

        //Relationship between Tournaments and TournamentGroup (( 1 TournamentGroup belong to 1 Tournament ))
        public virtual Tournament Tournament { get; set; }

        //Relationship between TournamentsGroup and TournamentTeam (( 1 TournamentGroup belong to * TournamentTeams ))
        public virtual ICollection<TournamentTeam> TournamentTeams { get; set; }

        //Relationship between TournamentsGroup and Match (( 1 TournamentGroup belong to * Matches ))
        public virtual ICollection<Match> Matches { get; set; }

        #endregion
    }
}
