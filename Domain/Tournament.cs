﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Tournament
    {
        #region Properties

        [Key]
        public int TournamentId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The maximum length for field {0} is {1} characters")]
        [Index("Tournament_Name_Index", IsUnique = true)]
        [Display(Name = "Tournament")]
        public string Name { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }

        [Display(Name = "Order")]
        public int Order { get; set; }

        //Relationship between Tournaments and TournamentGroup (( 1 Tournament belong to * TournamentGroup ))
        public virtual ICollection<TournamentGroup> Groups { get; set; }

        //Relationship between Tournaments and Date (( 1 Tournament belong to * Dates ))
        public virtual ICollection<Date> Dates { get; set; }
        #endregion
    }
}
