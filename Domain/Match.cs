﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Match
    {
        [Key]
        public int MatchId { get; set; }

        [Display(Name = "Date")]
        public int DateId { get; set; }

        [Display(Name = "Date time")]
        [DataType(DataType.DateTime)]
        public DateTime DateTime { get; set; }

        [Display(Name = "Local")]
        public int LocalId { get; set; }

        [Display(Name = "Visitor")]
        public int VisitorId { get; set; }

        [Display(Name = "Local goals")]
        public int? LocalGoals { get; set; }

        [Display(Name = "Visitor goals")]
        public int? VisitorGoals { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [Display(Name = "Group")]
        public int TournamentGroupId { get; set; }

        //Relationship between Match and Date (( 1 Match belong to 1 Date ))
        public virtual Date Date { get; set; }

        //Relationship between Match and Team (( 1 Match belong to 1 LocalTeam ))
        public virtual Team Local { get; set; }

        //Relationship between Match and Team (( 1 Match belong to 1 VisitorTeam ))
        public virtual Team Visitor { get; set; }

        //Relationship between Match and Status (( 1 Match belong to 1 Status ))
        public virtual Status Status { get; set; }

        //Relationship between Match and TournamentGroup (( 1 Match belong to 1 TournamentGroup ))
        public virtual TournamentGroup TournamentGroup { get; set; }

    }
}
